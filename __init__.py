import sys
import os
import bpy

def ensure_site_packages(packages):

    if not packages:
        return

    import site
    import importlib
    import importlib.util

    user_site_packages = site.getusersitepackages()
    os.makedirs(user_site_packages, exist_ok = True)
    sys.path.append(user_site_packages)

    modules_to_install = [module[1] for module in packages if not importlib.util.find_spec(module[0])]

    if modules_to_install:
        import subprocess

        if bpy.app.version < (2,91,0):
            python_binary = bpy.app.binary_path_python
        else:
            python_binary = sys.executable

        subprocess.run([python_binary, '-m', 'ensurepip'], check=True)
        subprocess.run([python_binary, '-m', 'pip', 'install', *modules_to_install, "--user"], check=True)

ensure_site_packages([
    ("pysolar", "pysolar")
])

bl_info = {
    "name": "Nishita Time",
    "description": "Geolocation and time to Blender Nishita sky texture",
    "author": "Kenny Jeffery",
    "version": (1, 0, 2),
    "blender": (2, 90, 0),
    "category": "Lighting"
}

from pysolar.solar import *
from calendar import monthrange
import datetime
import math

def validateDate(dateObject):
    if dateObject.dt[1] > 12:
        dateObject.dt[1] = 12
    day = monthrange(dateObject.dt[2], dateObject.dt[1])[1]
    if dateObject.dt[0] > day:
        dateObject.dt[0] = day

    return dateObject

def getSEA(latitude, longitude, date):
    hour = int(date.strftime("%H"))
    minute = int(date.strftime("%M"))
    # Check your timezone to add the offset
    day_of_year = int(date.strftime("%Y"))

    g = (360 / 365.25) * (day_of_year + hour / 24)

    g_radians = math.radians(g)

    declination = 0.396372 - 22.91327 * math.cos(g_radians) + 4.02543 * math.sin(g_radians) - 0.387205 * math.cos(
        2 * g_radians) + 0.051967 * math.sin(2 * g_radians) - 0.154527 * math.cos(3 * g_radians) + 0.084798 * math.sin(
        3 * g_radians)

    time_correction = 0.004297 + 0.107029 * math.cos(g_radians) - 1.837877 * math.sin(g_radians) - 0.837378 * math.cos(
        2 * g_radians) - 2.340475 * math.sin(2 * g_radians)

    SHA = (hour - 12) * 15 + longitude + time_correction

    if (SHA > 180):
        SHA_corrected = SHA - 360
    elif (SHA < -180):
        SHA_corrected = SHA + 360
    else:
        SHA_corrected = SHA

    lat_radians = math.radians(latitude)
    d_radians = math.radians(declination)
    SHA_radians = math.radians(SHA)

    SZA_radians = math.acos(
        math.sin(lat_radians) * math.sin(d_radians) + math.cos(lat_radians) * math.cos(d_radians) * math.cos(
            SHA_radians))

    SZA = math.degrees(SZA_radians)

    SEA = 90 - SZA

    return SEA

class Environment:

    envSet = False

    def setEnvironment(self):
        if self.envSet == False:
            C = bpy.context
            scn = C.scene
            scn.render.engine = 'CYCLES'
            node_tree = scn.world.node_tree
            tree_nodes = node_tree.nodes
            tree_nodes.clear()
            node_background = tree_nodes.new(type='ShaderNodeBackground')
            node_environment = tree_nodes.new('ShaderNodeTexSky')
            node_environment.location = -300,0
            node_output = tree_nodes.new(type='ShaderNodeOutputWorld')
            node_output.location = 200,0
            links = node_tree.links
            link = links.new(node_environment.outputs["Color"], node_background.inputs["Color"])
            link = links.new(node_background.outputs["Background"], node_output.inputs["Surface"])
            bpy.data.worlds["World"].node_tree.nodes["Sky Texture"].sky_type = 'NISHITA'
            self.envSet = True

class NishitaProperties(bpy.types.PropertyGroup):

    now = datetime.datetime.today()

    lat : bpy.props.FloatProperty(name= "Latitude", min= -90, max= 90, default= (0), precision= 6)

    lng : bpy.props.FloatProperty(name= "Longitude", min= -180, max= 180, default= (0), precision= 6)

    dt : bpy.props.IntVectorProperty(name= "Date", min= 1, max= 9999, default= (int(now.strftime("%d")),int(now.strftime("%m")),int(now.strftime("%Y"))))

    tm : bpy.props.IntVectorProperty(name= "Time", min= 0, max= 59, default= (int(now.strftime("%H")),int(now.strftime("%M")),int(now.strftime("%S"))))


class NISHITATIME_PT_main_panel(bpy.types.Panel):
    bl_label = "Main Panel"
    bl_idname = "NISHITATIME_PT_main_panel"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Nishita Time"

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        ntime = scene.ntime

        layout.prop(ntime, "lat")
        layout.prop(ntime, "lng")
        layout.prop(ntime, "dt")
        layout.prop(ntime, "tm")

        layout.operator("nishitatime.myop_operator")


class NISHITATIME_OT_my_op(bpy.types.Operator):
    bl_label = "Run"
    bl_idname = "nishitatime.myop_operator"
    environment = Environment()

    def execute(self, context):
        if self.environment.envSet == False:
            self.environment.setEnvironment()
        scene = context.scene
        ntime = scene.ntime
        utc = tzinfo=datetime.timezone.utc
        validateDate(ntime)
        if ntime.tm[0] > 23:
            ntime.tm[0] = 23
        date = datetime.datetime(ntime.dt[2], ntime.dt[1], ntime.dt[0], ntime.tm[0], ntime.tm[1], ntime.tm[2], 000000, utc)
        alt = get_altitude(ntime.lat, ntime.lng, date)
        rot = get_azimuth(ntime.lat, ntime.lng, date)
        el = getSEA(ntime.lat, ntime.lng, date)
        bpy.data.worlds["World"].node_tree.nodes["Sky Texture"].altitude = alt
        bpy.data.worlds["World"].node_tree.nodes["Sky Texture"].sun_rotation = math.radians(rot)
        bpy.data.worlds["World"].node_tree.nodes["Sky Texture"].sun_elevation = math.radians(el)

        return {'FINISHED'}


classes = [NishitaProperties, NISHITATIME_PT_main_panel, NISHITATIME_OT_my_op]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

        bpy.types.Scene.ntime = bpy.props.PointerProperty(type= NishitaProperties)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
        del bpy.types.Scene.ntime



if __name__ == "__main__":
    register()
