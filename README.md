# Blender Nishita Time
## Geolocation And Time To Nishita Sky Texture

This is an add on for [Blender](https://www.blender.org/) that transposes a real world geolocation (latitude/longitude coordinates) and time to the Nishita sky texture.

**Supported by Blender 2.9 and above.**

Nishita Time uses the [PySolar](https://pysolar.readthedocs.io/en/latest/) Python module and an amended version of this [script](https://github.com/mperezcorrales/SunPositionCalculator) for elevation calculation (as PySolar does not support it).

### Installation

Instructions for installing 3rd party add-ons into Blender can be found [here](https://docs.blender.org/manual/en/latest/editors/preferences/addons.html#rd-party-add-ons).
